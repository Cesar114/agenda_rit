package com.portillo.agenda;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.BundleCompat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    TableLayout tblLista;
    ArrayList<Contacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();

    }

    public void cargarContactos() {
        for(int x = 0; x < contactos.size(); x++) {
            final Contacto c= new Contacto(contactos.get(x));
            TableRow nRow=new TableRow(ListaActivity.this);
            TextView nText=new TextView(ListaActivity.this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorite())? Color.BLUE:Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListaActivity.this);
            nButton.setText(R.string.accion_ver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c=(Contacto)v.getTag(R.string.contacto_g);
                    Intent i= new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto",c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);setResult(RESULT_OK,i);
                    finish();
                }
            });


            nButton.setTag(R.string.contacto_g,c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            tblLista.addView(nRow);

        }
    }
}
